import psycopg2
import random
from faker import Faker


if __name__ == "__main__":
    fake = Faker()
    conn = psycopg2.connect(dbname='onesoil', user='postgres',
                            password='', host='localhost')
    conn.autocommit = True
    cursor = conn.cursor()
    nationality = ['Belarusians', 'Austrians', 'Korean', 'Germans', 'Basque']
    for n in nationality:
        for _ in range(1000):
            with conn.cursor() as cursor:
                sql_string = 'INSERT INTO "user" (name, nationality) VALUES (%s, %s) RETURNING id;'
                cursor.execute(sql_string, (fake.name(), n))
                id = cursor.fetchone()[0]
                sql_string = 'INSERT INTO "positions" (user_id, lat, lon) VALUES (%s, %s, %s);'
                cursor.execute(sql_string, (id, fake.latitude(), fake.longitude()))
