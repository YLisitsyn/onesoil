create table positions
(
  user_id int,
  lat float8,
  lon float8
);

select
       r.nationality,
       r.user_name_1,
       r.user_name_2,
       r.distanse distanse
from
     (
         select
            u1.nationality,
            u1.name user_name_1,
            u2.name user_name_2,
            earth_distance(ll_to_earth(u1.lat, u1.lon), ll_to_earth(u2.lat, u2.lon)) distanse,
            rank() OVER (PARTITION BY u1.nationality ORDER BY earth_distance(ll_to_earth(u1.lat, u1.lon), ll_to_earth(u2.lat, u2.lon)) DESC) rank
         from
             (select name, nationality, p.lat, p.lon from "user" join positions p on "user".id = p.user_id) u1,
             (select name, nationality, p.lat, p.lon from "user" join positions p on "user".id = p.user_id) u2
         where u1.nationality = u2.nationality and  u1.name != u2.name
     ) r
where r.rank <= 3;