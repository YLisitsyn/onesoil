create table "user"
(
  id serial not null,
  name varchar(255)
);

create unique index user_id_uindex
  on "user" (id);

alter table "user"
  add constraint user_pk
    primary key (id);
CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS $$
  DECLARE
    record RECORD;
    payload JSON;
  BEGIN
    IF (TG_OP = 'DELETE') THEN
      record = OLD;
    ELSE
      record = NEW;
    END IF;

    payload = json_build_object('table', TG_TABLE_NAME,
                                'action', TG_OP,
                                'data', row_to_json(record));

    PERFORM pg_notify('events', payload::text);

    RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER notify_order_event
AFTER INSERT OR UPDATE OR DELETE ON "user"
  FOR EACH ROW EXECUTE PROCEDURE notify_event();
LISTEN events;
