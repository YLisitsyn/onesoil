from simple_websocket_server import WebSocketServer, WebSocket

clients = []


class SimpleEcho(WebSocket):
    def handle(self):
        for client in clients:
            if client != self:
                client.send_message(self.data)

    def connected(self):
        print(self.address, 'connected')
        clients.append(self)

    def handle_close(self):
        print(self.address, 'closed')
        clients.remove(self)


server = WebSocketServer('0.0.0.0', 8555, SimpleEcho)
server.serve_forever()
