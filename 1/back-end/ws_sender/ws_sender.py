import websocket
import pg_channels
import os

db_host=os.environ.get('DBHOST')
db=os.environ.get('DB')
db_user=os.environ.get('DBUSER')
db_pass=os.environ.get('DBPASS')
ws_addr=os.environ.get('WS')

pgc = pg_channels.connect(host=db_host, database=db, user=db_user, password=db_pass)


def on_message(ws, message):
    pass


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    print("Connected")
    pgc.listen('events')

    for event in pgc.events():
        ws.send(event.payload)


if __name__ == "__main__":
    ws = websocket.WebSocketApp("ws://{}:8555".format(ws_addr),
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close)
    ws.on_open = on_open
    ws.run_forever()
